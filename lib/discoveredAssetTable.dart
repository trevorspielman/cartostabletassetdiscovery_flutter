import 'package:cartos_asset_tablet_discovery/assetProperties.dart';
import 'package:cartos_asset_tablet_discovery/main.dart';

import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';

class discoveredAssetTable extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Discovered Assets'), actions: <Widget>[
          new IconButton(
            icon: new Image.asset(
              'images/logo.png',
              height: 400,
              width: 400,
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyHomePage()));
            },
          ),
        ]),
        body: Center(
            child: Table(children: [
          TableRow(children: [
            Text("HP LaserJet", style: TextStyle(fontSize: 20)),
            Text("123456", style: TextStyle(fontSize: 20)),
            IconButton(
                icon: Icon(Icons.edit),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => assetProperties()));
                })
          ]),
          TableRow(children: [
            Text("Cannon PhotoBomb", style: TextStyle(fontSize: 20)),
            Text("123456", style: TextStyle(fontSize: 20)),
            IconButton(
                icon: Icon(Icons.edit),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => assetProperties()));
                })
          ]),
          TableRow(children: [
            Text("HP BlazorJet", style: TextStyle(fontSize: 20)),
            Text("123456", style: TextStyle(fontSize: 20)),
            IconButton(
                icon: Icon(Icons.edit),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => assetProperties()));
                })
          ]),
          TableRow(children: [
            Text("Sharp ImageSharp", style: TextStyle(fontSize: 20)),
            Text("123456", style: TextStyle(fontSize: 20)),
            IconButton(
                icon: Icon(Icons.edit),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => assetProperties()));
                })
          ]),
        ], border: TableBorder.all(color: Colors.black, width: 2))));
  }
}
