import 'package:cartos_asset_tablet_discovery/discoveredAssetTable.dart';
import 'package:cartos_asset_tablet_discovery/main.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';

class ProjectOverview extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Project Overview'), actions: <Widget>[
          new IconButton(
            icon: new Image.asset(
              'images/logo.png',
              height: 400,
              width: 400,
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyHomePage()));
            },
          ),
        ]),
        body: Center(
            child: Table(children: [
          TableRow(children: [
            Text("Project 1", style: TextStyle(fontSize: 20)),
            Text("10/20/2020", style: TextStyle(fontSize: 20)),
            IconButton(
                icon: Icon(Icons.edit),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => discoveredAssetTable()));
                })
          ]),
          TableRow(
            children: [
              Text("Project 2", style: TextStyle(fontSize: 20)),
              Text("10/10/2020", style: TextStyle(fontSize: 20)),
              IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => discoveredAssetTable()));
                  })
            ],
          ),
          TableRow(children: [
            Text("Project 3", style: TextStyle(fontSize: 20)),
            Text("10/1/2020", style: TextStyle(fontSize: 20)),
            IconButton(
                icon: Icon(Icons.edit),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => discoveredAssetTable()));
                })
          ]),
          TableRow(children: [
            Text("Project 4", style: TextStyle(fontSize: 20)),
            Text("10/30/2020", style: TextStyle(fontSize: 20)),
            IconButton(
                icon: Icon(Icons.edit),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => discoveredAssetTable()));
                })
          ]),
        ], border: TableBorder.all(color: Colors.black, width: 2))));
  }
}
