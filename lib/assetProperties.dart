import 'package:cartos_asset_tablet_discovery/main.dart';

import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';

class assetProperties extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Asset Properties'), actions: <Widget>[
          new IconButton(
            icon: new Image.asset(
              'images/logo.png',
              height: 400,
              width: 400,
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyHomePage()));
            },
          ),
        ]),
        body: Center(child: Text("This is the AssetProperties Window")));
  }
}
